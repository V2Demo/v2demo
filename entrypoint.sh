#!/usr/bin/env sh

# Global variables
DIR_CONFIG="/etc/v2ray"
DIR_RUNTIME="/usr/bin"
DIR_TMP="$(mktemp -d)"

###################################
CONFIG_JSON_FILE_NAME=v2demo.json
CONFIG_PB_FILE_NAME=v2demo.pb
RUNTIME_LOG_FILE_NAME=v2demo.log
###################################

# Write V2Demo configuration
cat << EOF > ${DIR_TMP}/${CONFIG_JSON_FILE_NAME}
{
    "inbounds": [{
        "port": ${PORT},
        "protocol": "vmess",
        "settings": {
            "clients": [{
                "id": "${ID}",
                "alterId": ${AID}
            }],
            "disableInsecureEncryption": true
        },
        "streamSettings": {
            "network": "ws",
            "wsSettings": {
                "path": "${WSPATH}"
            }
        }
    }],
    "outbounds": [{
        "protocol": "freedom"
    }]
}
EOF

# Get V2Demo executable release
curl --retry 10 --retry-max-time 60 -H "Cache-Control: no-cache" -fsSL github.com/v2fly/v2ray-core/releases/latest/download/v2ray-linux-64.zip -o ${DIR_TMP}/v2ray_dist.zip
busybox unzip ${DIR_TMP}/v2ray_dist.zip -d ${DIR_TMP}

# Convert to protobuf format configuration
mkdir -p ${DIR_CONFIG}
${DIR_TMP}/v2ctl config ${DIR_TMP}/${CONFIG_JSON_FILE_NAME} > ${DIR_CONFIG}/${CONFIG_PB_FILE_NAME}

# Install V2Demo
install -m 755 ${DIR_TMP}/v2ray ${DIR_RUNTIME}
rm -rf ${DIR_TMP}

# Run V2Demo
${DIR_RUNTIME}/v2ray -config=${DIR_CONFIG}/${CONFIG_PB_FILE_NAME} > ${DIR_RUNTIME}/${RUNTIME_LOG_FILE_NAME} 2>&1 &