# V2Demo
> [![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://dashboard.heroku.com/new?template=https://V2Demo@bitbucket.org/V2Demo/v2demo.git)

| Variable | Default Value |
| :--- | :--- |
| `ID` | `ad806487-2d26-4636-98b6-ab85cc8521f7` |
| `AID` | `64` |
| `WSPATH` | `/` |
